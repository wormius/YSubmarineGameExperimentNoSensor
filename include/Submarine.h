#ifndef SUBMARINE_H
#define SUBMARINE_H

#include <SFML/Graphics.hpp>

class Submarine : public sf::Sprite
{
    public:
        Submarine(float , float);
        virtual ~Submarine();

        void        update(float, float);


    private:

        sf::Texture         texture;
        sf::Vector2f        v;
};

#endif // SUBMARINE_H
