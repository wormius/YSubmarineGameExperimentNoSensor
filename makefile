CPP = g++

CPPFLAGS=-std=c++11 

CCPINCLUDES=-Iinclude/

CCPLDFLAGS=-lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system 


VPATH=./src ./include ./obj
vpath %.cpp ./src


YSubmarineGame :YSubmarineGame.o  Evil.o Eye.o Game.o Ground.o Pins.o Submarine.o
	$(CPP) $(CPPFLAGS)  $^ -o $@ $(CCPLDFLAGS)


%.o : %.cpp
	$(CPP) $(CPPFLAGS) $(CCPINCLUDES) -c $^ -o $@
	

.PHONY: clean
clean:
	rm -r YSubmarineGame *.o