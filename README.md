# YSubmarineGameExperimentNoSensor

YSubmarineGameExperimentNoSensor es un un clon de juego [YSubmarineGameExperiment](https://gitlab.com/wormius/YSubmarineGameExperiment)  pero sin la necesidad de ensamblar ningun dispositivo.

El juego YSubmarineGameExperimentNoSensor está escrito en C++ y utiliza las librería SFML.

***
![YSubmarineGameExperimentNoSensor](https://gitlab.com/wormius/YSubmarineGameExperimentNoSensor/raw/master/Screenshot.png  "YSubmarineGameExperimentNoSensor")
***


## Requisitos previos a la compilación de YSubmarineGameExperimentNoSensor

Para compilar y obtener el ejecutable, se necesita tener instalados:

- make
- g++
- SFML (libsfml-dev)


### Instalación de requisitos

Para instalar **make**, **g++** y **SFML**:

En Ubuntu y derivados:
~~~
sudo apt-get install make build-essential libsfml-dev
~~~


## Compilación y ejecución de YSubmarineGameExperimentNoSensor

### Compilación
Escribir en terminal, dentro de la capeta de YSubmarineGameExperimentNoSensor:
~~~
make
~~~

### Ejecución
Despues de compilar, escribir en terminal, dentro de la capeta de YSubmarineGameExperimentNoSensor:
~~~
./YSubmarineGame
~~~

## Cómo jugar YSubmarineGameExperimentNoSensor


- Ejecutar el juego.
- Presionar la **barra espaciadora** para iniciar el juego.
- Presionar la **barra espaciadora** para impulsar verticalmelte el submarino y evitar los obstaculo.



