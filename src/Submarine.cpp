#include "Submarine.h"

Submarine::Submarine(float x, float y):
    sf::Sprite(),
    texture(),
    v(0,0)
{
    texture.loadFromFile("sources/ys.png");
    this->setTexture(texture);
    this->setOrigin(sf::Vector2f(this->getLocalBounds().width/2.0,this->getLocalBounds().height/2.0));
    this->setPosition(sf::Vector2f(x,y));
}

Submarine::~Submarine()
{
    //dtor
}

void Submarine::update(float vel, float dt)
{



    
    
    if (v.y > 350) v.y = 350;
    else if (v.y < -150) v.y = -150;
    else v.y += vel;


    this->setPosition(this->getPosition().x, this->getPosition().y - dt*v.y);



    if (this->getPosition().y > 720-20-this->getLocalBounds().height/2.0)
    {
        this->setPosition(this->getPosition().x, 720-20-this->getLocalBounds().height/2.0);
    }

    if (this->getPosition().y < 20+this->getLocalBounds().height/2.0)
    {
        this->setPosition(this->getPosition().x, 20+this->getLocalBounds().height/2.0);
    }



}
