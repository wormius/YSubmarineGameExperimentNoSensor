#include "Game.h"

Game::Game():
    WIDTH(1280),
    HEIGHT(720),
    window(sf::VideoMode(1280,720), "YSubmarineGame"),
    time(sf::Time::Zero),
    dt(sf::Time::Zero),
    dt_fps(sf::Time::Zero),
    fps(60),
    font_n(),
    submarine(200, HEIGHT/2.0),
    ground(),
    top_ground(),
    vel(0),
    running(false),
    ds(5.0),//***************************************Level
    ddx(2.0),//***************************************Level
    ddy(0.0),//***************************************Level
    mark(0),
    dt_mark(sf::Time::Zero),
    evil(),
    eye(),
    tpin(),
    teye(),
    tevil(),
    tblock(),
    r(183),
    g(200),
    b(196),
    dt_color(sf::Time::Zero),
    lose(false)

{
    window.setFramerateLimit(60);

    font_n.loadFromFile("sources/LeagueMono-ExtraBold.ttf");

    music.openFromFile("sources/sub.ogg");
    music.setLoop(true);
    music.setVolume(100);

    boom_bff.loadFromFile("sources/boom.ogg");
    boom_snd.setBuffer(boom_bff);
    boom_snd.setLoop(false);
    boom_snd.setVolume(100);

    tpin.loadFromFile("sources/pins.png");
    tevil.loadFromFile("sources/evil2.png");
    teye.loadFromFile("sources/eye2.png");
    tblock.loadFromFile("sources/ground.png");

    lose_sub.loadFromFile("sources/ls.png");
    lose_sub2.loadFromFile("sources/ls2.png");


    for (int i = 0; i <= 34; i++)
    {
        ground.push_back(new Ground(i*40,HEIGHT, tblock));

    }

    for (int i = 0; i <= 34; i++)
    {
        top_ground.push_back(new Ground(i*40,20, tblock));

    }

    for (int i = 0; i <= 10; i++)
    {
        pins.push_back(new Pins(2000+i*150,HEIGHT-20,tpin));

    }

    for (int i = 0; i <= 10; i++)
    {
        top_pins.push_back(new Pins(2000+(i+1)*150,20,tpin));
        top_pins[top_pins.size()-1]->setRotation(180);

    }

    for (int i = 0; i <= 5; i++)
    {
        evil[i] = new Evil(sf::Vector2f(2400+ i*350.f,150 + std::rand()%(420)),tevil);//Leveeeeeel******************
        eye[i] = new Eye(evil[i]->getPosition());
    }




    std::srand(0);

    smark.setFont(font_n);
    smark.setCharacterSize(100);
    smark.setString(ToString(mark));
    smark.setOrigin(smark.getLocalBounds().width/2,0);
    smark.setPosition(WIDTH/2.0, 70);






}

Game::~Game()
{

    for (int i = 0; i < ground.size(); i++)
    {
        delete ground[i];

    }

    for (int i = 0; i < top_ground.size(); i++)
    {
        delete top_ground[i];

    }

    for (int i = 0; i < pins.size(); i++)
    {
        delete pins[i];

    }

    for (int i = 0; i < top_pins.size(); i++)
    {
        delete top_pins[i];

    }

    for (int i = 0; i <= 5; i++)
    {
        delete evil[i] ;
        delete eye[i] ;
    }

}
 
void Game::run()
{
    clock.restart();
    
    


    while (window.isOpen())
    {
        sf::Event       event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) window.close();

            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Escape) window.close();
                
            }
            

        }// on event

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) and not running and not lose)
        {
            running = true;
            dt_mark = sf::Time::Zero;
            music.play();
        }

        if (running && dt_fps.asSeconds() >= 1.0f/fps )
        {

            if (dt_mark.asMilliseconds() >= 1500)
            {
                mark ++;
                dt_mark = sf::Time::Zero;
            }
            
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
            {
                submarine.update(10, dt.asSeconds());
            }
            else submarine.update(-5, dt.asSeconds());


            //

            for (int i = 0; i <= 5; i++)
            {
                evil[i]->move(-ddx,ddy);
                eye[i]->update(evil[i]->getPosition(), submarine.getPosition());

                if (evil[i]->getGlobalBounds().intersects(submarine.getGlobalBounds()))
                {
                    sf::Vector2f        dr = evil[i]->getPosition() - submarine.getPosition();
                    float norm = std::sqrt(std::pow(dr.x,2)+std::pow(dr.y,2));
                    if (norm <= 130)
                    {
                        lose = true;
                        running = false;
                        submarine.setTexture(lose_sub, true);
                        submarine.setOrigin(submarine.getLocalBounds().width/2.0,-submarine.getLocalBounds().height/2.0);
                        music.stop();
                        boom_snd.play();
                    }

                }

            }

            for (int i = 0; i <= 5; i++)
            {

                if (evil[i]->getPosition().x <= -200)
                {

                    evil[i]->setPosition(sf::Vector2f(2200,150 + std::rand()%(420)));

                }


            }


            for (int i = 0; i < ground.size(); i++)
            {
                ground[i]->move(-ds,0);

                if (ground[i]->getPosition().x <= -40)
                {
                    delete ground[i];
                    ground.pop_front();

                    ground.push_back(new Ground(ground[ground.size()-1]->getPosition().x+ 40,
                                                ground[ground.size()-1]->getPosition().y,
                                                tblock));
                }

            }


            for (int i = 0; i < top_ground.size(); i++)
            {
                top_ground[i]->move(-ds,0);

                if (top_ground[i]->getPosition().x <= -40)
                {
                    delete top_ground[i];
                    top_ground.pop_front();

                    top_ground.push_back(new Ground(top_ground[top_ground.size()-1]->getPosition().x+ 40,
                                                top_ground[top_ground.size()-1]->getPosition().y,
                                                tblock));
                }

            }

            for (int i = 0; i < pins.size(); i++)
            {
                pins[i]->move(-ds,0);
                if(pins[i]->getGlobalBounds().intersects(submarine.getGlobalBounds()))
                {

                    lose = true;
                    running = false;
                    submarine.setTexture(lose_sub2, true);
                    submarine.setOrigin(submarine.getLocalBounds().width/2.0,-submarine.getLocalBounds().height/2.0);
                    music.stop();
                    boom_snd.play();

                }
                if (pins[i]->getPosition().x <= -150)
                {
                    delete pins[i];
                    pins.pop_front();

                    pins.push_back(new Pins(pins[pins.size()-1]->getPosition().x+ 150,
                                                pins[pins.size()-1]->getPosition().y,
                                                tpin));
                }

            }


            for (int i = 0; i < top_pins.size(); i++)
            {
                top_pins[i]->move(-ds,0);
                if(top_pins[i]->getGlobalBounds().intersects(submarine.getGlobalBounds()))
                {

                    lose = true;
                    running = false;
                    submarine.setTexture(lose_sub, true);
                    submarine.setOrigin(submarine.getLocalBounds().width/2.0,-submarine.getLocalBounds().height/2.0);
                    music.stop();
                    boom_snd.play();

                }
                if (top_pins[i]->getPosition().x <= -150)
                {
                    delete top_pins[i];
                    top_pins.pop_front();

                    top_pins.push_back(new Pins(top_pins[top_pins.size()-1]->getPosition().x+ 150,
                                                top_pins[top_pins.size()-1]->getPosition().y,
                                                tpin));

                    top_pins[top_pins.size()-1]->setRotation(180);
                }

            }


            if (dt_color.asSeconds() > 0.1 and time.asSeconds() < 65)
            {
                if (b > 0) b--;
                else if (g > 0) g--;
                else if (r > 0) r--;

                dt_color = sf::Time::Zero;

            }
            else if (dt_color.asSeconds() > 0.1)
            {
                if (b >= 0 && b <= 193) b++;
                else if (g >= 0 && g <=200) g++;
                else if (r >= 0 && r<=183) r++;

                dt_color = sf::Time::Zero;

                if (r == 183) time = sf::Time::Zero;

            }





        }//if running




        smark.setString(ToString(mark));
        smark.setOrigin(smark.getLocalBounds().width/2.0,smark.getLocalBounds().height/2.0);
        smark.setPosition(WIDTH/2.0, 70);

        if (lose)
        {
            submarine.update(-5, dt.asSeconds());

        }



        if (dt_fps.asSeconds() >= 1.0f/fps )
        {





            window.clear(sf::Color(r,g,b));




            window.draw(submarine);

            for (int i = 0; i  <= 5; i++)
            {
                window.draw(*evil[i]);
                window.draw(*eye[i]);

            }

            for (int i = 0; i  < ground.size(); i++)
            {
                window.draw(*ground[i]);

            }

            for (int i = 0; i  < top_ground.size(); i++)
            {
                window.draw(*top_ground[i]);

            }

            for (int i = 0; i  < pins.size(); i++)
            {
                window.draw(*pins[i]);

            }

            for (int i = 0; i  < top_pins.size(); i++)
            {
                window.draw(*top_pins[i]);

            }





            window.draw(smark);



            window.display();

            dt_fps = sf::Time::Zero;

        }// on draw



        dt = clock.restart();

        dt_fps += dt;
        dt_mark += dt;
        dt_color += dt;
        time += dt;



        if (mark >= 80)
        {
            ddx = 3.0+3.0*std::sin(3.0*time.asSeconds());
            ddy = std::sin(2.0*time.asSeconds());

        }
        else if (mark >=60 )
        {


            ddx = 4.0f+2*std::sin(time.asSeconds());
            ddy = std::sin(2.0*time.asSeconds());


        }
        else if (mark >=45)
        {
            ddy = 2.0f*std::sin(time.asSeconds());

        }
        else if (mark >=25)
        {


            ddy = 1.5*std::sin(5.0*time.asSeconds());


        }



    } //main_loop


}

std::string     Game::ToString(float value)
{
    std::ostringstream ss;
    ss << value;
    return ss.str();
}


