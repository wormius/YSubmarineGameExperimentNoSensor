#include "Evil.h"

Evil::Evil(sf::Vector2f position, sf::Texture &texture):
    sf::Sprite()
{
    texture.setSmooth(true);
    this->setTexture(texture);
    this->setOrigin(this->getLocalBounds().width/2.0,this->getLocalBounds().height/2.0);
    this->setPosition(position);

}

Evil::~Evil()
{
    //dtor
}

