#ifndef GAME_H
#define GAME_H

#include <sstream>
#include <string>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "Submarine.h"

#include "Ground.h"

#include "Evil.h"

#include "Eye.h"

#include "Pins.h"

#include <deque>

#include <cstdlib>


class Game
{
public:
    Game();
    virtual ~Game();

    void       run();

private:

    sf::RenderWindow        window;
    sf::Clock               clock;
    sf::Time                time;
    sf::Time                dt;
    sf::Time                dt_fps;
    sf::Time                dt_mark;
    sf::Time                dt_color;


    int                     fps;

    sf::Font                font_n;

    sf::Text                smark;


    int                     WIDTH;
    int                     HEIGHT;



    std::string         ToString(float value);



    Submarine               submarine;


    std::deque<Ground*>             ground;
    std::deque<Ground*>             top_ground;
    std::deque<Pins*>               pins;
    std::deque<Pins*>               top_pins;


    Evil*               evil[6];
    Eye*               eye[6];


    float               vel;

    bool                running;

    float                 ds;
    float                 ddx;
    float                 ddy;

    int                 mark;



    sf::Texture                     tpin;
    sf::Texture                     teye;
    sf::Texture                     tevil;
    sf::Texture                     tblock;



    sf::Music                       music;
    sf::SoundBuffer                 boom_bff;
    sf::Sound                       boom_snd;


    int                             r;
    int                             g;
    int                             b;


    bool                            lose;
    sf::Texture                     lose_sub;
    sf::Texture                     lose_sub2;







};

#endif // GAME_H
